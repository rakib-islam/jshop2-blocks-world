package convert;

/**
 * Created by mislam1 on 8/31/16.
 */
public class Location {
    public int x;
    public int y;

    public Location() {
        x = 0;
        y = 0;
    }

    public Location(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public Location(Location location) {
        this.x = location.x;
        this.y = location.y;
    }
}
