package convert;

/**
 * Created by mislam1 on 8/30/16.
 */
public class SpatialRelation {
    int direction; //0 - north, 1 - south, 2 - east, 3 - west
    String staticLocation ;
    String variableLocation;
    int distance ;

     public SpatialRelation () {
         direction = -1;
         staticLocation = "";
         variableLocation = "";
         distance = 0;
     }

    public SpatialRelation (int direction, int distance, String staticLocation, String variableLocation) {
        this.direction = direction;
        this.distance = distance;
        this.staticLocation = staticLocation;
        this.variableLocation = variableLocation;
    }
}
