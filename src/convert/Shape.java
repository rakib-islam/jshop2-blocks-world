package convert;

/**
 * Created by mislam1 on 8/30/16.
 */
public class Shape {
    public int height;
    public int width;
    public int startingX;
    public int startingY;
    public int endingX;
    public int endingY;
    public String shapeName;
    public String identifier;
    public String block;

    public Shape () {
        height = 0;
        width = 0;
        startingX = 0;
        startingY = 0;
        endingX = 0;
        endingY = 0;
        shapeName = "";
        identifier = "";
        block = "";
    }

    public Shape (int height, int width, int startingX, int startingY, String shapeName, String identifier) {
        this.height = height;
        this.width = width;
        this.startingX = startingX;
        this.startingY = startingY;
        this.endingX = -1;
        this.endingY = -1;
        this.shapeName = new String(shapeName);
        this.identifier = new String(identifier);
        this.block = "";
    }



}
