package convert;

/**
 * Created by mislam1 on 8/30/16.
 */
public class CombinedInformation {
    public String identifier;
    public String block;
    public String location;

    public CombinedInformation() {
        identifier = "";
        block = "";
        location = "";
    }

    public CombinedInformation(String identifier, String block, String location) {
        this.identifier = new String(identifier);
        this.block = new String(block);
        this.location = new String(location);
    }

    @Override
    public String toString() {
        return "identifier: " + this.identifier + " block: " + this.block + " location: " + this.location;
    }
}
