package convert;

import java.io.*;
import java.util.*;

/**
 * Created by mislam1 on 8/30/16.
 */
public class SemanticRepresentationtoPDDL {

    public List<String> predicateList = null;
    public int numberOfShapes;
    public List<String> shapesNameList = null;
    public List<Shape> shapesList = null;
    public HashMap<String, Shape> shapesMap = null;
    public List<SpatialRelation> spatialRelationList = null;
    public HashSet<String> blockLocationSet = null;
    Map<String, CombinedInformation> locationCombinedMap = null;

    public SemanticRepresentationtoPDDL() {
        predicateList = new ArrayList<>();
        shapesNameList = new ArrayList<>();
        shapesList = new ArrayList<>();
        shapesMap = new HashMap<>();
        spatialRelationList = new ArrayList<>();
        numberOfShapes = 0;
        blockLocationSet = new HashSet<>();
        locationCombinedMap = new HashMap<>();
    }

    public void initiate (String str) {
        predicateList = new ArrayList<>();
        shapesNameList = new ArrayList<>();
        shapesList = new ArrayList<>();
        shapesMap = new HashMap<>();
        spatialRelationList = new ArrayList<>();
        numberOfShapes = 0;
        blockLocationSet = new HashSet<>();
        locationCombinedMap = new HashMap<>();

        String[] splittedStrings = str.replace(" ","").split("\\^");

        for (int i = 0; i < splittedStrings.length; ++i) {
            String predicate = splittedStrings[i];
            if (predicate.startsWith("row")) {
                shapesNameList.add(new String("row"));
                ++numberOfShapes;
            } else if (predicate.startsWith("column")) {
                shapesNameList.add(new String("column"));
                ++numberOfShapes;
            } else if (predicate.startsWith("rectangle")) {
                shapesNameList.add(new String("rectangle"));
                ++numberOfShapes;
            } else if (predicate.startsWith("square")) {
                shapesNameList.add(new String("square"));
                ++numberOfShapes;
            }
            predicateList.add(new String(splittedStrings[i]));
        }
    }

    public void CreateShapes () {
        for (int i = 0; i < predicateList.size(); ++i) {
            String currentPredicate = predicateList.get(i);
            if (currentPredicate.startsWith("row")) {
                String identifier = currentPredicate.split("\\(")[1].replace(")","");
                String shapeName = "row";
                currentPredicate = predicateList.get(++i);
                if (currentPredicate.startsWith("width")) {
                    int width = Integer.parseInt(currentPredicate.split(",")[1].replace(")",""));
                    Shape shape = new Shape(0, width, 0, 0, shapeName, identifier);
                    shapesList.add(shape);
                    shapesMap.put(identifier, shape);
                }
            } else if (currentPredicate.startsWith("column")) {
                String identifier = currentPredicate.split("\\(")[1].replace(")","");
                String shapeName = "column";
                currentPredicate = predicateList.get(++i);
                if (currentPredicate.startsWith("height")) {
                    int height = Integer.parseInt(currentPredicate.split(",")[1].replace(")",""));
                    Shape shape = new Shape(height, 0, 0, 0, shapeName, identifier);
                    shapesList.add(shape);
                    shapesMap.put(identifier, shape);
                }
            } else if (currentPredicate.startsWith("rectangle")) {
                String identifier = currentPredicate.split("\\(")[1].replace(")","");
                String shapeName = "rectangle";
                int height = 0;
                int width = 0;
                currentPredicate = predicateList.get(++i);
                if (currentPredicate.startsWith("height")) {
                    height = Integer.parseInt(currentPredicate.split(",")[1].replace(")",""));
                }
                currentPredicate = predicateList.get(++i);
                if (currentPredicate.startsWith("width")) {
                    width = Integer.parseInt(currentPredicate.split(",")[1].replace(")",""));
                }
                Shape shape = new Shape(height, width, 0, 0, shapeName, identifier);
                shapesList.add(shape);
                shapesMap.put(identifier, shape);
            } else if (currentPredicate.startsWith("square")) {
                String identifier = currentPredicate.split("\\(")[1].replace(")","");
                String shapeName = "square";
                currentPredicate = predicateList.get(++i);
                int height = 0;
                if (currentPredicate.startsWith("size")) {
                    height = Integer.parseInt(currentPredicate.split(",")[1].replace(")",""));
                }
                Shape shape = new Shape(height, height, 0, 0, shapeName, identifier);
                shapesList.add(shape);
                shapesMap.put(identifier, shape);
            }
        }
    }


    public void ShapesSpatialRelation () {
        for (int i = 0; i < predicateList.size(); ++i) {
            String currentPredicate = predicateList.get(i);
            if (currentPredicate.contains("spatial-rel")) {
                String[] splittedString = currentPredicate.replaceAll(" ","").split("\\(|\\)|,");
                int direction = -1;
                if (splittedString[1].equals("north")) {
                    direction = 0;
                } else if (splittedString[1].equals("south")) {
                    direction = 1;
                } else if (splittedString[1].equals("east")) {
                    direction = 2;
                } else if (splittedString[1].equals("west")) {
                    direction = 3;
                }
                blockLocationSet.add(new String(splittedString[3]));
                blockLocationSet.add(new String(splittedString[4]));
                spatialRelationList.add(new SpatialRelation(direction,
                        Integer.parseInt(splittedString[2]), splittedString[3], splittedString[4]));

            }
        }
    }

    public void ResolveStartingPoint () {
        // TODO: 8/31/16 iterate over the locationCombinedMap, thenfind the direction east, west ...
        // TODO: 8/31/16 get the enum index in grid and also the start and end point of the current shape
        // TODO: 8/31/16
        boolean first = false;
        if (shapesList.size() == 1) {
            Shape shape = shapesList.get(0);
            shape.startingX = 0;
            shape.startingY = 0;
            ComputeEndIndex(shape);
        } else {
            for (SpatialRelation s : spatialRelationList) {
                String staticLocation = s.staticLocation;
                String variableLocation = s.variableLocation;
                int direction = s.direction;
                if (!first) {
                    first = true;
                    Shape shape = shapesMap.get(locationCombinedMap.get(staticLocation).identifier);
                    shape.startingX = 0;
                    shape.startingY = 0;
                    ComputeEndIndex(shape);
                    int index = EnumRelationIndex(shape, locationCombinedMap.get(staticLocation).block);
                    Location location = GetGridfromIndex(shape, index);
                    Shape shapeVariable = shapesMap.get(locationCombinedMap.get(variableLocation).identifier);
                    int indexVariable = EnumRelationIndex(shapeVariable, locationCombinedMap.get(variableLocation).block);
                    Location locationVariable = null;
                    if (direction == 0) {
                        //north
                        locationVariable = new Location(location.x, location.y - 1);
                    } else if (direction == 1) {
                        //south
                        locationVariable = new Location(location.x, location.y + 1);
                    } else if (direction == 2) {
                        //east
                        locationVariable = new Location(location.x + 1, location.y);

                    } else if (direction == 3) {
                        //west
                        locationVariable = new Location(location.x - 1, location.y);

                    }
                    Location spVariable = ResolveBlockStartingLocationFromIndexLoction(shapeVariable, indexVariable, locationVariable);
                    shapeVariable.startingX = spVariable.x;
                    shapeVariable.startingY = spVariable.y;
                    ComputeEndIndex(shapeVariable);

                } else {
                    Shape shape = shapesMap.get(locationCombinedMap.get(staticLocation).identifier);
                    int index = EnumRelationIndex(shape, locationCombinedMap.get(staticLocation).block);
                    Location location = GetGridfromIndex(shape, index);
                    Shape shapeVariable = shapesMap.get(locationCombinedMap.get(variableLocation).identifier);
                    int indexVariable = EnumRelationIndex(shapeVariable, locationCombinedMap.get(variableLocation).block);
                    Location locationVariable = null;
                    if (direction == 0) {
                        //north
                        locationVariable = new Location(location.x, location.y - 1);
                    } else if (direction == 1) {
                        //south
                        locationVariable = new Location(location.x, location.y + 1);
                    } else if (direction == 2) {
                        //east
                        locationVariable = new Location(location.x + 1, location.y);

                    } else if (direction == 3) {
                        //west
                        locationVariable = new Location(location.x - 1, location.y);
                    }
                    Location spVariable = ResolveBlockStartingLocationFromIndexLoction(shapeVariable, indexVariable, locationVariable);
                    shapeVariable.startingX = spVariable.x;
                    shapeVariable.startingY = spVariable.y;
                    ComputeEndIndex(shapeVariable);
                }
            }
        }

    }

    public Location ResolveBlockStartingLocationFromIndexLoction (Shape shape, int index, Location location) {
        Location returnLocation = null;
        if (index == 0) returnLocation = new Location(location);
        if (shape.shapeName.equals("row")) {
            if (index == (shape.width - 1)) {
                returnLocation = new Location(location.x - shape.width - 1, location.y);
            }
            else if ( index < (shape.width - 1)) {
                returnLocation = new Location(location.x - index, location.y );
            }
        } else if (shape.shapeName.equals("column")) {
            if (index == (shape.height - 1)) {
                returnLocation = new Location(location.x, location.y - shape.height - 1);
            }
            else if ( index < (shape.height - 1)) {
                returnLocation = new Location(location.x, location.y - index);
            }
        } else if (shape.shapeName.equals("rectangle") || shape.shapeName.equals("square")) {
            int incX = index % shape.width;
            int incY = (int) Math.floor(index / shape.width);
            returnLocation = new Location(location.x - incX, location.y - incY);
        }
        return returnLocation;
    }

    public void ResolveIndetifierBlockLocation () {

        int blockLocationSize = blockLocationSet.size();
        List <String> blockLocationList = new ArrayList<>(blockLocationSet);
        for (int i = 0; i < blockLocationSize; ++i) {
            String currentLocation = blockLocationList.get(i);
            String block = GetBlock(currentLocation);
            String identifier = GetIdentifier(block);
            locationCombinedMap.put(currentLocation, new CombinedInformation(identifier, block, currentLocation));
        }
    }


    public int EnumRelationIndex (Shape shape, String block) {
        for (int i = 0; i < predicateList.size(); ++i) {
            String currentPredicate = predicateList.get(i);
            if (currentPredicate.contains("enum") && currentPredicate.contains(shape.identifier)
                    && currentPredicate.contains(block)) {
                return Integer.parseInt(currentPredicate.replace(" ", "").split("\\(|\\)|,")[3]);
            }
        }
        return -1;
    }


    public void ComputeEndIndex (Shape s) {
        if (s.shapeName.equals("row")) {
            s.endingX = s.startingX + s.width - 1;
            s.endingY = s.startingY;
        } else if (s.shapeName.equals("column")) {
            s.endingX = s.startingX;
            s.endingY = s.startingY + s.height - 1;
        } else if (s.shapeName.equals("rectangle") || s.shapeName.equals("square")) {
            s.endingX = s.startingX + s.width - 1;
            s.endingY = s.startingY + s.height - 1;
        }
    }

    public Location GetGridfromIndex (Shape shape, int index) {
        Location location = null;
        if (shape.shapeName.equals("row")) {
            location = new Location(shape.startingX + index, shape.startingY);
        } else if (shape.shapeName.equals("column")) {
            location = new Location(shape.startingX, shape.startingY + index);
        } else if (shape.shapeName.equals("rectangle")) {
            int incX = index % shape.width;
            int incY = (int) Math.floor(index / shape.width);
            location = new Location(shape.startingX + incX, shape.startingY + incY);
        } else if (shape.shapeName.equals("square")) {
            int incX = index % shape.width;
            int incY = (int) Math.floor(index / shape.width);
            location = new Location(shape.startingX + incX, shape.startingY + incY);
        }
        return location;
    }

    public String GetIdentifier (String block) {
        String identifier = "";
        for ( String s : predicateList) {
            if (s.contains("enum") && s.contains(block)) {
                identifier = s.replaceAll(" ","").split("\\(|\\)|,")[1];
            }
        }
        return identifier;
    }

    public String GetBlock (String location) {
        String block = "";
        for ( String s : predicateList) {
            if (s.contains("block-location") && s.contains(location)) {
                block = s.replaceAll(" ","").split("\\(|\\)|,")[1];
            }
        }
        return block;
    }

    public void PrintShapes () {
        System.out.println("===shapes===");
        for (Shape s : shapesList) {
            System.out.println("Name: " + s.shapeName);
            System.out.println("Height: " + s.height);
            System.out.println("Width: " + s.width);
            System.out.println("identifier: " + s.identifier);
            System.out.println("startX: " + s.startingX);
            System.out.println("startY: " + s.startingY);
            System.out.println("endX: " + s.endingX);
            System.out.println("endY: " + s.endingY);
        }
        System.out.println();
    }

    public void PrintSpatialRelations () {
        System.out.println("===SpatialRelations===");
        for (SpatialRelation s : spatialRelationList) {
            System.out.println("direction: " + s.direction);
            System.out.println("static: " + s.staticLocation);
            System.out.println("variable: " + s.variableLocation);
        }
        System.out.println();
    }

    public void PrintLocationCombinationMap () {
        System.out.println("===LocationCombinationMap===");
        System.out.println(locationCombinedMap);
        System.out.println();
    }


    public void CreateProblemPDDL(int blocksCount, int index) {
        int initialX = 100;
        int initialY = 100;

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/Users/mislam1/Documents/RA/DARPA/jshop2-master/examples/blocks/cwc-" + index));
            String header = "(defproblem smallproblem cwc\n" +
                    " (\n";
            bufferedWriter.write(header);
            for (int i = 0; i < blocksCount; ++i) {
                bufferedWriter.write("(block b" + (i+1) + " " + initialX + " " + (initialY + i) + ")\n");
                bufferedWriter.write("(on-table b" + (i+1) + ")(clear b" + (i+1)+ ")\n");
            }
            bufferedWriter.write(")\n");

            String problemHeader = "(\n" +
                    "  (achieve-goals\n" +
                    "   (";
            bufferedWriter.write(problemHeader);

            int blockIndex = 1;
            for (Shape s : shapesList) {
                if (s.shapeName.equals("row")) {
                    for (int i = s.startingX; i <= s.endingX; ++i) {
                        bufferedWriter.write("(block b" + (blockIndex) + " " + i + " " + s.startingY + ")( clear b" + (blockIndex) + ")\n");
                        blockIndex++;
                    }
                } else if (s.shapeName.equals("column")) {
                    for (int i = s.startingY; i <= s.endingY; ++i) {
                        bufferedWriter.write("(block b" + (blockIndex) + " " + s.startingX + " " + i + ")( clear b" + (blockIndex) + ")\n");
                        blockIndex++;
                    }
                } else if (s.shapeName.equals("square") || s.shapeName.equals("rectangle")) {
                    for (int i = s.startingY; i <= s.endingY; ++i) {
                        for (int j = s.startingX; j <= s.endingX; ++j) {
                            bufferedWriter.write("(block b" + (blockIndex) + " " + j + " " + i + ")( clear b" + (blockIndex) + ")\n");
                            blockIndex++;
                        }
                    }
                }
            }

            String problemCloser = ")\n" +
                    "  )\n" +
                    " )\n" +
                    ")";
            bufferedWriter.write(problemCloser);
            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public int NumberOfBlocksNeeded () {
        int blocksCount = 0;
        for (Shape s : shapesList) {
            if (s.shapeName.equals("row")) {
                blocksCount += s.width;
            } else if (s.shapeName.equals("column")) {
                blocksCount += s.height;
            } else if (s.shapeName.equals("square")) {
                blocksCount += ( s.height * s.width);
            } else if (s.shapeName.equals("rectangle")) {
                blocksCount += (s.width * s.height) ;
            }
        }
        return blocksCount;
    }


    public void ProcessAll () {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("sem.txt"));
            String line ;
            int lineCount = 0;
            while ( (line = bufferedReader.readLine()) != null) {
                initiate(line);
                CreateShapes();
                ShapesSpatialRelation();
                ResolveIndetifierBlockLocation();
                PrintLocationCombinationMap();
                ResolveStartingPoint();
                int blocksCount = NumberOfBlocksNeeded();
                CreateProblemPDDL(blocksCount, lineCount++);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void main (String[] args) {

        String semRep = "row(a) ^ width(a, 5) ^ square(b) ^ size(b, 4) ^ block(c) ^ location(w1) ^ block-location(c, w1) ^ enum(b, c, 0) ^ block(d) ^ location(w2) ^ block-location(d, w2) ^ enum(a, d, 1) ^ spatial-rel(south, 0, w2, w1)";
                //"rectangle(a) ^ height(a, 4) ^ width(a, 2)";
                //"row(a) ^ width(a, 5) ^ square(b) ^ size(b, 4) ^ block(c) ^ location(w1) ^ block-location(c, w1) ^ enum(b, c, 0) ^ block(d) ^ location(w2) ^ block-location(d, w2) ^ enum(a, d, 1) ^ spatial-rel(south, 0, w2, w1)";
                //"row(a) ^ width(a, 5)";
                /*"row(a) ^ width(a, 4) ^ rectangle(b) ^ height(b, 3) ^ width(b, 2) ^ block(c) ^ location(w1) " +
                "^ block-location(c, w1) ^ enum(b, c, 4) ^ block(d) ^ location(w2) ^ block-location(d, w2) " +
                "^ enum(a, d, 2) ^ spatial-rel(north, 0, w2, w1) ^ square(e) ^ size(e, 3) ^ block(f) ^ location(w3) " +
                "^ block-location(f, w3) ^ enum(e, f, 8) ^ block(g) ^ location(w4) ^ block-location(g, w4) ^ enum(b, g, 0) " +
                "^ spatial-rel(north, 0, w4, w3)";*/

        SemanticRepresentationtoPDDL semanticRepresentationtoPDDL = new SemanticRepresentationtoPDDL();
        semanticRepresentationtoPDDL.ProcessAll();
        /*semanticRepresentationtoPDDL.initiate(semRep);
        semanticRepresentationtoPDDL.CreateShapes();
        //semanticRepresentationtoPDDL.PrintShapes();
        semanticRepresentationtoPDDL.ShapesSpatialRelation();
        //semanticRepresentationtoPDDL.PrintSpatialRelations();
        semanticRepresentationtoPDDL.ResolveIndetifierBlockLocation();
        semanticRepresentationtoPDDL.PrintLocationCombinationMap();
        semanticRepresentationtoPDDL.ResolveStartingPoint();
        //semanticRepresentationtoPDDL.PrintShapes();
        int blocksCount = semanticRepresentationtoPDDL.NumberOfBlocksNeeded();
        semanticRepresentationtoPDDL.CreateProblemPDDL(blocksCount);*/



    }
}
