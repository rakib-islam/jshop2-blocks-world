import JSHOP2.Plan;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

public class gui{
	public static void main(String[] args) {
		smallproblem.getPlans();
		//System.out.println(smallproblem.getPlans());
		try {
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("output.txt", true));
			LinkedList<Plan> plans = smallproblem.getPlans();
			for (Plan p : plans) {
				boolean first = false;
				String planString = p.toString();
				String [] splittedSteps = planString.split("\n");
				for (int i = 0; i < splittedSteps.length; ++i) {
					if (splittedSteps[i].contains("putdown")) {
						bufferedWriter.write(splittedSteps[i].replaceAll("\\!", "") + ", ");
					}
				}
				bufferedWriter.write("\n");
				bufferedWriter.write("===\n");
			/*if (planString.contains("pickup") || planString.contains("putdown")) {
				System.out.println("=====");
				//System.out.println("--------" +planString);
			}*/
			}
			bufferedWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}



		//new JSHOP2GUI();
	} 
}
