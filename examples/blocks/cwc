;;; This file contains a SHOP domain representation of the block-stacking
;;; algorithm from the following paper:
;;;    N. Gupta and D. Nau, On the complexity of blocks-world planning,
;;;    Artificial Intelligence 56(2-3):223-254, August 1992.


;;; ------------------------------------------------------------------------
;;; Declare all the data
;;; ------------------------------------------------------------------------

(defdomain cwc
  (
    ;; basic block-stacking operators

    (:operator (!pickup ?b ?x ?y)
               ()
               ((clear ?b) (on-table ?b))
               ((holding ?b)))

    (:operator (!putdown ?b ?x ?y)
               ()
               ((holding ?b))
               ((on-table ?b) (clear ?b)))

    (:operator (!stack ?b ?x ?y)
               ()
               ((holding ?b) (clear ?b))
               ((block ?b ?x ?y) (clear ?b)))

    (:operator (!unstack ?b ?x ?y)
               ()
               ((clear ?b) (on-table ?b))
               ((holding ?b)))

    ;; book-keeping methods & ops, to keep track of what needs to be done
    (:operator (!!assert ?g)
               ()
               ()
               (?g)
               ;; Since !!ASSERT isn't a real blocks-world operator, make its cost 0
               0)

    (:operator (!!remove ?g)
               ()
               (?g)
               ()
               ;; Since !!REMOVE isn't a real blocks-world operator, make its cost 0
               0)

    ;; The method for the top-layer task
    (:method (achieve-goals ?goals)
             ()
             ((assert-goals ?goals)
             (find-nomove) (add-new-goals) (find-movable) (move-block)))

    (:method (assert-goals (?goal . ?goals))
             ()
             ((!!assert (goal ?goal))
              (assert-goals ?goals)))

    (:method (assert-goals nil)
             ()
             ())

    ;; Find those blocks which don't need to be moved.
    ;; This is called once in the beginning of the process.
    ;; Blocks in their final positions are distinguished with a
    ;; dont-move predicate in the world state
    (:method (find-nomove)
             ((block ?x) (not (dont-move ?x)) (not (need-to-move ?x)))
             ((!!assert (dont-move ?x)) (find-nomove))
             nil
             nil)

    ;; Find blocks with no assosiated goals and add an appropriate goal
    ;; (on-table ?x) for those blocks if they have to be moved, i.e. if
    ;; they are on the way of something else. Otherwise, we can simply
    ;; ignore them
    (:method (add-new-goals)
             ((block ?x) (not (dont-move ?x)) (not (goal (on-table ?x)))
             (not (goal (on ?x ?y))))
             ;Decomposition
             ((!!assert (goal (on-table ?x))) (add-new-goals))

             nil
             nil)

    ;; Find all those blocks which can be moved to their final position
    ;; directly in the initial state of the world. Such blocks are marked
    ;; with either a put-on-table predicate or a stack-on-block predicate,
    ;; depending on their associated goal
    (:method (find-movable)
             ((clear ?b) (not (dont-move ?b))
             (goal (block ?b ?x ?y)) (not (put-on-table ?b ?x ?y)))
             ; Decomposition
             ((!!assert (put-on-table ?b ?x ?y)) (find-movable))

             ((clear ?x) (not (dont-move ?x)) (goal (on ?x ?y))
             (not (stack-on-block ?x ?y)) (dont-move ?y) (clear ?y))
             ;Decomposition
             ((!!assert (stack-on-block ?x ?y)) (find-movable))

             nil
             nil)

    ;; Check if the thing that is supposed to end up on top of ?x is ready
    ;; to go there. This is called whenever we move block ?x to its final
    ;; position.
    (:method (check ?b ?x2 ?y2)
             ((goal (on-table ?b)) (goal (block ?b ?x2 ?y2)) (clear ?b))
             ((!!assert (palced-on-block ?b ?x2 ?y2)))
             nil
             nil)

    ;; Check if the thing that is supposed to end up on top of ?x is ready
    ;; to go there. This is called whenever something is removed from top
    ;; of the ?x. Note that here, we must check if ?x is in final position,
    ;; while in the latter method we were sure that it was and thus we did
    ;; not need a verification.
    (:method (check2 ?x)
             ((dont-move ?x) (goal (on ?y ?x)) (clear ?y))
             ((!!assert (stack-on-block ?y ?x)))
             nil
             nil)

    ;; Check if x can go to where it is supposed to end up. This is again
    ;; called whenever something is removed from top of the ?x, making it
    ;; able to move around.
    (:method (check3 ?x)
             (dont-move ?x)
             nil
             ((goal (on ?x ?y)) (clear ?y) (dont-move ?y))
             ((!!assert (stack-on-block ?x ?y)))
             ((goal (on-table ?x)))
             ((!!assert (put-on-table ?x ?a ?b)))
             nil
             nil)

    (:method (check4 ?b)
             (moving ?b)
             ((!!assert (on-table ?b)) (!!assert (block ?b ?x1 ?y1))
             ((goal (on-table ?b)) (goal(block ?b ?x ?y)) (clear ?b) (moving ?b))
             (!! remove (block ?b ?x1 ?y1)))

             )

    ;; Just an efficiency trick, to avoid calculating things twice
    ;; This trick is a general technique while working with SHOP. If there
    ;; are several possible decompositions for a task and they have some
    ;; preconditions in common, one can "factor" those preconditions and
    ;; add a new level in the task hierarchy whose precondition is the
    ;; set of common preconditions. This way, one may avoid calculating
    ;; the shared preconditions for several times. Here, the stack-on-block
    ;; is the shared precondition in the move-block method
    (:method (move-block1 ?b ?x1 ?y1 ?x2 ?y2)
             ;method-for-moving-b-from-x-to-z
             ;((on-table ?b) (block ?b ?x1 ?y1))
             ;Decomposition
             ;((
             ;(!!assert (move ?x))
             ;(!!remove (put-on-table ?b ?x2 ?y2))
             ;)

             method-for-moving-b-from-x1-y1-to-x2-y2
             ((on-table ?b) (block ?b ?x1 ?y1))
             ; Decomposition
             ((!pickup ?b ?x1 ?y1) (!stack ?b ?x2 ?y2)

             (!!remove (put-on-table ?b ?x1 ?y1))
             (check ?b ?x2 ?y2)))

    ;; This is the main method. It first moves the blocks that are directly
    ;; movable to their final positions to their final position. Doing so
    ;; may make other blocks directly movable to their final positions.
    ;; Thus this method checks such possibilities using methods check, check1
    ;; and check2 and then calls itself to simulate an iteration. If there is
    ;; no such block, it means that we are done with the planning (A direct
    ;; result of the fact that blocks world problems are always solvable).
    (:method (move-block)
             ;((put-on-table ?b ?x2 ?y2))
             ;((move-block1 ?b ?x1 ?y1 ?x2 ?y2) (move-block))

             method-for-moving-x-from-y-to-table
             ((put-on-table ?b ?x ?y) (on-table ?b) (block ?b ?x1 ?y1))
             ;Decomposition
             ((!pickup ?b ?x1 ?y1) (!putdown ?b ?x ?y)
             (!!assert (moving ?b))
             (!!remove (put-on-table ?b ?x ?y))
              (move-block))

             ;method-for-moving-x-out-of-the-way
             ;((clear ?b) (not (dont-move ?b)) (block ?b ?x1 ?y1))
             ;Decomposition
             ;((!unstack ?x ?y) (!putdown ?b ?x ?y)
             ;(check ?b ?x2 ?y2)(move-block))

             termination-method-branch
             nil
             nil)

    ;; state axioms

    (:- (same ?x ?x) nil)

    ;; Finds the blocks that must be moved, because they are blocking other
    ;; blocks' way.
    (:- (need-to-move ?x)
        ;; need to move x if x needs to go from one block to another
        ((on ?x ?y) (goal (on ?x ?z)) (not (same ?y ?z)))
        ;; need to move x if x needs to go from table to block
        ((on-table ?x) (goal (on ?x ?z)))
        ;; need to move x if x needs to go from block to table
        ((on ?x ?y) (goal (on-table ?x)))
        ;; need to move x if x is on y and y needs to be clear
        ((on ?x ?y) (goal (clear ?y)))
        ;; need to move x if x is on z and something else needs to be on z
        ((on ?x ?z) (goal (on ?y ?z)) (not (same ?x ?y)))
        ;; need to move x if x is on something else that needs to be moved
        ((on ?x ?w) (need-to-move ?w))
    )
    ;;(:method (rectangle ?x1 ?y1 ?x2 ?y2)
    ;;  ((row ?x1 ?y1 ?x2 ?y1)(call <= ?y1 ?y2)) (rectangle ?x1 ?y1 + 1 ?x2 ?y2))

    ;;(!put_block ?b ?x1 ?y1)(row ( call + ?x1 1) ?y1 ?x2 ?y1) (call <= ?x1 ?x2))

     (:method (row ?x1 ?y1 ?x2 ?y1)
      ((call <= ?x1 ?x2))
      ((!put_block ?b ?x1 ?y1) (row ( call + ?x1 1) ?y1 ?x2 ?y1))
     )

      ;(:method (create-shape)
      ;  ()
      ;  ((row ?x1 ?y1 ?x2 ?y1))
      ;  ())

     ;;(:method (column ?x1 ?y1 ?x1 ?y2)
     ;; if (?y1 <= ?y2)
     ;;   put_block (?b ?x1 ?y1) (column ?x1 ?y1 + 1 ?x1 ?y2))

  )
)


